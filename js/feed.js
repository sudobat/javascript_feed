var Feed = function(user) {
	this.user 		= user;
	this.messages 	= [];
};

Feed.prototype.addMessage = function(message) {
	this.messages.push(message);
};

Feed.prototype.removeMessage = function(id) {
	this.messages = this.messages.filter(function(message) {
		if (message.id == id) {
			return false;
		}

		return true;
	});
};

Feed.prototype.getMessage = function(id) {
	var result = this.messages.filter(function(message) {
		if (message.id == id) {
			return true;
		}

		return false;
	});

	return result[0];
};

Feed.prototype.getAllMessages = function() {
	return this.messages;
};

Feed.prototype.updateMessage = function(id, new_content) {
	var message = this.getMessage(id);

	// Por si no existe el mensaje
	if (typeof(message) === "undefined") return;

	message.update(new_content);
};