var Message = function(id, content) {
	this.id 		= id;
	this.content 	= content;
};

Message.prototype.update = function(new_content) {
	this.content = new_content;
};

Message.prototype.excerpt = function() {
	return this.content.substr(0, 20);
};